#!/bin/sh

export INFILE="$1"

pdfjam --fitpaper 'true' --doublepagestwist 'true' -o work/back.pdf -- "$INFILE" -
pdfjam --frame 'true' --landscape --nup '1x2' -o work/out.pdf -- work/back.pdf -
#pdfjam --nup '1x4' -o work/out-tmp.pdf --no-tidy -- work/out.pdf -

export BASENAME=$(basename "$INFILE" .pdf)
export OUTFILE="$BASENAME.flipbook.pdf"
#mv work/out.pdf "out/$OUTFILE"
#test -f "out/$OUTFILE" && echo "Wrote out/$OUTFILE"

