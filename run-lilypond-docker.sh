#!/bin/bash

sudo chmod 777 /srv
sudo rm -f /srv/tunes.ly || exit 1
sudo cp "$1" /srv/tunes.ly
sudo rm -f /srv/tunes.pdf
sudo docker run -v /srv:/srv/ruby/lilypond:z airdock/lilypond:latest tunes.ly
sudo cp /srv/tunes.pdf "$2"
sudo chmod 755 /srv
test -f "$2" && echo Wrote "$2"
