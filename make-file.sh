#!/bin/sh

rm work/*

export INFILE="$1"
shift
echo '' | python ./formatter.py -p preamble/a4x4/full.ly.fragment -i "$INFILE" -o work/tunes.ly -v "$@" - 2>work/fail.abc

export BASENAME=$(basename "$INFILE" .abc)
export OUTFILE="$BASENAME.pdf"
./run-lilypond-docker.sh work/tunes.ly "$OUTFILE"

#pdfjam --landscape --nup '1x2' -o work/out-tmp.pdf -- work/out.pdf -
#pdfjam --nup '1x4' -o work/out-tmp.pdf --no-tidy -- work/out.pdf -
